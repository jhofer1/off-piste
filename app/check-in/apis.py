from importlib import import_module
from logging import getLogger

from flask import Blueprint, jsonify
from quotes import Quotes

api_name = 'check-in'
url_prefix = '/{}'.format(api_name)

# [Project Gnar]: This variable must be `blueprint` for compatibility with `gnar-gear`
blueprint = Blueprint(api_name, __name__, url_prefix=url_prefix)

main = import_module('off-piste.app.main')
log = getLogger(__name__)
q = Quotes()


@blueprint.route('', methods=['GET'])
def check_in():
    quote = q.random()
    message = main.app.send_sqs_message('gnar-queue', '{} --{}'.format(quote[1], quote[0]))
    return jsonify('Off-Piste checking in! '
                   'Check back with Piste at /10-45/get-message/{} in 1 minute for a secret message.'
                   .format(message['MessageId']))
